<!-- Header -->
<header class="w3-container" style="padding-top:22px">
  <h5><b><i id="membership" class="fa"></i> Cluster Membership</b></h5>
</header>
<!-- END Header -->

<!-- Cluster Members -->
<div class="w3-container">
  <h5>Cluster Members</h5>
  <table id="clusterMembers" class="w3-table w3-striped w3-bordered w3-border w3-hoverable w3-white">
    <thead><th>Member Name</th><th>Member IP Address</th><th>Member Ping</th></thead>
  </table>
  <br>
</div>
<!-- END Cluster Members -->